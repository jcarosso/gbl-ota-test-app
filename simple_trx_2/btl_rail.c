#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "config/btl_config.h"

//#include "btl_comm_xmodem.h"
#include "btl_rail.h"

#include "btl_interface.h"

#include "core/btl_bootload.h"
#include "core/btl_reset.h"
#include "driver/btl_driver_delay.h"
#include "driver/btl_driver_uart.h"
//#include "plugin/debug/btl_debug.h"
//#include "plugin/communication/btl_communication.h"

#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_emu.h"

// Parser
//#include "plugin/parser/btl_image_parser.h"
//#include "plugin/security/btl_security_types.h"

#include "rail.h"
#include "rail_types.h"
#include "rail_config.h"

#include "bsp.h"

// Memory manager configuration
#define TX_FIFO_SIZE  (512)  // Any power of 2 from [64, 4096] on the EFR32

// BSP
#define LED_TX (0)
#define LED_RX (1)

// RAIL state information
static RAIL_Handle_t railHandle = NULL;
//FORNOW static RAIL_TxPower_t txPower = 200; // Default to 20 dBm
static uint8_t txFifo[TX_FIFO_SIZE];

// RAIL configuration information
static RAIL_Config_t railCfg = {
  .eventsCallback = &RAILCb_Generic,
};

// SS2 bootloader state information
static uint8_t channel = 2;
static volatile uint8_t packetRx = true;   //go into receive mode
static uint16_t  FWDL_blocks_received[BTL_FWDL_MAX_BLOCKS];

/*
// EBL parser state information
static ParserContext_t parserContext;
static DecryptContext_t decryptContext;
static AuthContext_t authContext;
const uint8_t parserFlags = 0;

static ImageProperties_t imageProps = {
  .imageContainsBootloader = false,
  .imageContainsApplication = false,
  .imageCompleted = false,
  .imageVerified = false,
  .bootloaderVersion = 0,
  .application = { 0 }
};
*/
static const BootloaderParserCallbacks_t parseCb = {
  .context = NULL,
  .applicationCallback = bootload_applicationCallback,
  .metadataCallback = NULL,
  .bootloaderCallback = bootload_bootloaderCallback
};

// SS2 bootloader tx/rx packets

static  BTL_Message_t BTL_TX_Message;

void btl_radioInit()
{
	  railHandle = RAIL_Init(&railCfg, NULL);
	  if (railHandle == NULL) {
	    while (1) ;
	  }
	  RAIL_ConfigCal(railHandle, RAIL_CAL_ALL);

	  // Set us to a valid channel for this config and force an update in the main
	  // loop to restart whatever action was going on
	  RAIL_ConfigChannels(railHandle, channelConfigs[0], NULL);

	  // Configure RAIL callbacks
	  RAIL_ConfigEvents(railHandle,
	                    RAIL_EVENTS_ALL,
	                    (RAIL_EVENT_RX_PACKET_RECEIVED
	                     | RAIL_EVENT_TX_PACKET_SENT
	                     | RAIL_EVENT_TX_UNDERFLOW
	                     | RAIL_EVENT_RX_FRAME_ERROR));

	  // Initialize the PA now that the HFXO is up and the timing is correct
	  RAIL_TxPowerConfig_t txPowerConfig = {
	#if HAL_PA_2P4_LOWPOWER
	    .mode = RAIL_TX_POWER_MODE_2P4_LP,
	#else
	    .mode = RAIL_TX_POWER_MODE_2P4_HP,
	#endif
	    .voltage = HAL_PA_VOLTAGE,
	    .rampTime = HAL_PA_RAMP,
	  };
	  if (channelConfigs[0]->configs[0].baseFrequency < 1000000UL) {
	    // Use the Sub-GHz PA if required
	    txPowerConfig.mode = RAIL_TX_POWER_MODE_SUBGIG;
	  }
	  if (RAIL_ConfigTxPower(railHandle, &txPowerConfig) != RAIL_STATUS_NO_ERROR) {
	    // Error: The PA could not be initialized due to an improper configuration.
	    // Please ensure your configuration is valid for the selected part.
	    while (1) ;
	  }

	  RAIL_SetTxPower(railHandle, HAL_PA_POWER);

	  RAIL_SetTxFifo(railHandle, txFifo, 0, TX_FIFO_SIZE);
	  RAIL_SetFixedLength(railHandle, sizeof(BTL_Message_t));

	  // Set automatic transitions to always receive once started
    RAIL_StateTransitions_t railStateTransitions = {
      .success = RAIL_RF_STATE_RX,
      .error   = RAIL_RF_STATE_RX,
    };
	  RAIL_SetRxTransitions(railHandle, &railStateTransitions);
	  RAIL_SetTxTransitions(railHandle, &railStateTransitions);

	  RAIL_Idle(railHandle, RAIL_IDLE, true);

	  // Start RAIL receiver
	  btl_rail_rx();

	  char* str = "\r\nRAIL Initialized\r\n";
	  printf(str);//uart_sendBuffer((uint8_t*)str, strlen(str), true);
}

void btl_rail_tx(uint8_t* dataPtr)
{
  //RAIL_Idle(railHandle, RAIL_IDLE, true);
  RAIL_WriteTxFifo(railHandle, dataPtr, sizeof(BTL_TX_Message), false);
  RAIL_StartTx(railHandle, channel, RAIL_TX_OPTIONS_DEFAULT, NULL);
}

void btl_rail_rx(void)
{
	RAIL_StartRx(railHandle, channel, NULL);
  packetRx = false;
}

bool btl_packet_rx(void)
{
  return packetRx;
}

void btl_ping(void)
{
  memset(&BTL_TX_Message, 0, sizeof(BTL_TX_Message));
  BTL_TX_Message.MsgType = SS2_Ping;
  BTL_TX_Message.Msg.Ping.Data = 42;
  btl_rail_tx((uint8_t*)&BTL_TX_Message);
}

void btl_FWDL_tx_initiate(void)
{
  memset(&BTL_TX_Message, 0, sizeof(BTL_TX_Message));
  BTL_TX_Message.MsgType = SS2_FWDL_State;
  BTL_TX_Message.Msg.State.State = FWDL_INITIATE;
  btl_rail_tx((uint8_t*)&BTL_TX_Message);
}

void btl_FWDL_tx_complete(void)
{
  memset(&BTL_TX_Message, 0, sizeof(BTL_TX_Message));
  BTL_TX_Message.MsgType = SS2_FWDL_State;
  BTL_TX_Message.Msg.State.State = FWDL_COMPLETE;
  btl_rail_tx((uint8_t*)&BTL_TX_Message);
}

void btl_FWDL_tx_code_block(void* pBlock, uint8_t length, uint16_t blockNum)
{
  memset(&BTL_TX_Message, 0, sizeof(BTL_TX_Message));
  BTL_TX_Message.MsgType = SS2_CodeBlock;
  BTL_TX_Message.Msg.CodeBlock.BlockNumber = blockNum;
  BTL_TX_Message.Msg.CodeBlock.TotalBlocks = 0;
  BTL_TX_Message.Msg.CodeBlock.Length = length;
  memcpy(BTL_TX_Message.Msg.CodeBlock.Payload, pBlock, MIN(length, BTL_FWDL_BLOCK_SIZE));
  btl_rail_tx((uint8_t*)&BTL_TX_Message);
}

char *  _EXFUN(itoa,(int, char *, int));

/******************************************************************************
 * RAIL Callback Implementation
 *****************************************************************************/
void RAILCb_Generic(RAIL_Handle_t railHandle, RAIL_Events_t events)
{
  int32_t ret = BOOTLOADER_OK;

  (void)railHandle;

  if (events & RAIL_EVENT_RX_PACKET_RECEIVED) {
    BSP_LedToggle(LED_RX);
    packetRx = true;
    char str[10];

    BTL_Message_t btlMessage;
    uint16_t rx_bytes = RAIL_ReadRxFifo(railHandle, (uint8_t*)&btlMessage, sizeof(btlMessage));

    if (rx_bytes != sizeof(btlMessage))
    {
      printf("?\r\n");
      return;
    }

    switch (btlMessage.MsgType)
    {
    case SS2_Ping:
      printf("p");//uart_sendBuffer((uint8_t*)"p", 1, true);
      break;
    case SS2_FWDL_State:
      switch (btlMessage.Msg.State.State)
      {
      case FWDL_INITIATE:
        BSP_LedSet(LED_RX);
        BSP_LedClear(LED_TX);
        ret = bootloader_eraseStorageSlot(0);
        if (ret == BOOTLOADER_OK)
        {
          printf("I\r\n");//uart_sendBuffer((uint8_t*)"I\r\n", 3, true);
          memset(FWDL_blocks_received, 0xFF, sizeof(FWDL_blocks_received));
        }
        break;
      case FWDL_COMPLETE:
        BSP_LedSet(LED_RX);
        BSP_LedSet(LED_TX);
        ret = bootloader_verifyImage(0, (BootloaderParserCallback_t)NULL);
        if (ret == BOOTLOADER_OK)
        {
          printf("C\r\n");//uart_sendBuffer((uint8_t*)"C\r\n", 3, true);
          bootloader_rebootAndInstall();
        }
        else
        {
          printf("X\r\n");//uart_sendBuffer((uint8_t*)"X\r\n", 3, true);
        }
        break;
      default:
        break;
      }
      break;
    case SS2_CodeBlock:
      if (FWDL_blocks_received[btlMessage.Msg.CodeBlock.BlockNumber] == 0xFFFF)
      {
      ret = bootloader_writeStorage(
          0,
          btlMessage.Msg.CodeBlock.BlockNumber * BTL_FWDL_BLOCK_SIZE,
          btlMessage.Msg.CodeBlock.Payload,
          btlMessage.Msg.CodeBlock.Length);
      if (ret == BOOTLOADER_OK)
      {
        BSP_LedToggle(LED_TX);
        if (btlMessage.Msg.CodeBlock.BlockNumber < BTL_FWDL_MAX_BLOCKS)
        {
          FWDL_blocks_received[btlMessage.Msg.CodeBlock.BlockNumber] = btlMessage.Msg.CodeBlock.BlockNumber;
          itoa(btlMessage.Msg.CodeBlock.BlockNumber, str, 10);
          printf(str);//uart_sendBuffer((uint8_t*)str, strlen(str), true);
          printf("\r\n");//uart_sendBuffer((uint8_t*)"\r\n", 2, true);
        }
      }
      }
      break;
    default:
      break;
    }
  }
  if (events & RAIL_EVENT_TX_PACKET_SENT) {
    BSP_LedToggle(LED_TX);
  }
  if (events & RAIL_EVENT_TX_UNDERFLOW) {
    printf("u\r\n");//uart_sendBuffer((uint8_t*)"u\r\n", 3, true);
  }
  if (events & RAIL_EVENT_RX_FRAME_ERROR) {
    printf("x\r\n");//uart_sendBuffer((uint8_t*)"x\r\n", 3, true);
  }
}
