/*
 * btl_rail.h
 *
 */
#include "rail_types.h"

#ifndef XMODEM_UART_BTL_RAIL_H_
#define XMODEM_UART_BTL_RAIL_H_

// RAIL interface
//
void btl_rail_init(void);
void btl_config_payload(uint8_t* data, uint16_t datalen);
void btl_rail_tx(uint8_t* data);
void btl_rail_rx(void);
bool btl_packet_rx(void);

// FWDL interface
//
#ifndef MIN
#define MIN(a,b) ((a)<(b)?(a):(b))
#endif
#define BTL_FWDL_BLOCK_SIZE 128 // FORNOW matches xmodem implementation
#define BTL_FWDL_MAX_BLOCKS 768

// Firmware D/L states
typedef enum
{
  FWDL_NONE,
  FWDL_INITIATE,    // Tell target to expect FWDL
  FWDL_CODE_BLOCK,  // Send 1 code block
  FWDL_COMPLETE,    // Ask target for f/w validation
  FWDL_RESTART,     // Reboot target (unused)
  FWDL_ABORT,       // Abort FWDL (unused)
} FWDL_State_t;

typedef enum
{
  SS2_Ping = 1,
  SS2_FWDL_State,
  SS2_CodeBlock,
} SS2_MsgType_t;

typedef struct
{
  uint8_t Data;
} BTL_Ping_t;

typedef struct
{
  FWDL_State_t State;
} BTL_FWDLState_t;

typedef struct
{
  uint16_t BlockNumber; // 0-N
  uint16_t TotalBlocks; // TODO calculate this
  uint16_t Length;
  uint8_t Payload[BTL_FWDL_BLOCK_SIZE];
} BTL_FWDLCodeBlock_t;

// RAIL interface messages
//
typedef struct
{
  SS2_MsgType_t         MsgType;
  union
  {
    BTL_Ping_t          Ping;
    BTL_FWDLState_t     State;
    BTL_FWDLCodeBlock_t CodeBlock;
  } Msg;
} BTL_Message_t;

// Static prototypes
void RAILCb_Generic(RAIL_Handle_t railHandle, RAIL_Events_t events);
void btl_radioInit();

// Global prototypes
void btl_ping(void);
void btl_FWDL_tx_initiate(void);
void btl_FWDL_tx_complete(void);
void btl_FWDL_tx_code_block(void* pBlock, uint8_t length, uint16_t blockNum);

#endif /* XMODEM_UART_BTL_RAIL_H_ */
