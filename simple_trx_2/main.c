/***************************************************************************//**
 * @file main.c
 * @brief Application specific overrides of weak functions defined as part of
 * the test application.
 * @copyright Copyright 2015 Silicon Laboratories, Inc. http://www.silabs.com
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rail.h"
#include "rail_types.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_emu.h"
#include "bsp.h"
#include "gpiointerrupt.h"
#include "rail_config.h"
#include "hal_common.h"

#include "btl_xmodem.h"
#include "btl_driver_delay.h"
#include "btl_driver_uart.h"
#include "btl_interface.h"

#include "core/btl_bootload.h"
#include "core/btl_reset.h"

#include "application_properties.h"

// Parser
#include "plugin/parser/btl_image_parser.h"
//#include "plugin/security/btl_security_types.h"

#include "btl_rail.h"

extern const ApplicationProperties_t applicationProperties;

// Memory manager configuration
#define MAX_BUFFER_SIZE  256

// Minimum allowed size of the TX FIFO
#define RAIL_TX_FIFO_SIZE 64

// General application memory sizes
#define APP_MAX_PACKET_LENGTH  (MAX_BUFFER_SIZE - 12) /* sizeof(RAIL_RxPacketInfo_t) == 12) */

// Prototypes
void radioInit();
void gpioCallback(uint8_t pin);

RAIL_Handle_t railHandle = NULL;

typedef struct ButtonArray{
  GPIO_Port_TypeDef   port;
  unsigned int        pin;
} ButtonArray_t;

SL_PACK_START(1)
typedef struct {
  uint8_t padding;
  XmodemPacket_t packet;
} SL_ATTRIBUTE_PACKED XmodemReceiveBuffer_t;
SL_PACK_END()

typedef enum {
  MENU,
  IDLE,
  INIT_TRANSFER,
  WAIT_FOR_DATA,
  RECEIVE_DATA,
  BOOT,
  COMPLETE,
  SS2_PING,
  SS2_FWDL_INITIATE,
  SS2_FWDL_BLOCK_RX,
  SS2_FWDL_COMPLETE,
  SS2_FWDL_RESTART,
} XmodemState_t;

static const ButtonArray_t buttonArray[BSP_NO_OF_BUTTONS] = BSP_GPIO_BUTTONARRAY_INIT;

uint8_t channel = 0;
volatile bool packetTx = false; //go into transfer mode
volatile bool packetRx = true;  //go into receive mode
int32_t ret = BOOTLOADER_OK;
XmodemState_t state = IDLE;
XmodemReceiveBuffer_t buf;
uint8_t response;
int timeout = 60;
static uint16_t FWDL_block_number = 0;

static uint8_t transmitData[RAIL_TX_FIFO_SIZE] = {
  0x0F, 0x16, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66,
  0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE,
};

#define LED_TX (0)
#define LED_RX (1)

void menu(void)
{
  uint32_t version = mainBootloaderTable->header.version;
  printf("\r\nSwimSafe2 BL Test App v%d.%d.%d\r\n"
               "1. upload gbl (via XMODEM)\r\n"
               "2. run\r\n"
               "3. ping\r\n"
               "4. FWDL initiate\r\n"
               "5. FWDL complete\r\n"
               "STRX> ",
               (int)(version >> 24) & 0x0F,
               (int)(version >> 16) & 0x0F,
               (int)(version & 0x0F));
}

static int32_t sendPacket(uint8_t packet)
{
  int32_t ret;
  ret = uart_sendByte(packet);

  if (packet == XMODEM_CMD_CAN) {
    // If packet is CAN, send three times
    uart_sendByte(packet);
    uart_sendByte(packet);
  }

  return ret;
}

static int32_t receivePacket(XmodemPacket_t *packet)
{
  int32_t ret = BOOTLOADER_OK;
  size_t requestedBytes;
  size_t receivedBytes;
  uint8_t *buf = (uint8_t *)packet;

  // Wait for bytes to be available in RX buffer
  while (uart_getRxAvailableBytes() == 0) {
    // Do nothing
  }
  // TODO: Timeout at some point?

  // Read the first byte
  requestedBytes = 1;
  ret = uart_receiveBuffer(buf,
                           requestedBytes,
                           &receivedBytes,
                           true,
                           1000);

  if (packet->header != XMODEM_CMD_SOH) {
    // All packets except XMODEM_CMD_SOH are single-byte
    return BOOTLOADER_OK;
  }

  requestedBytes = sizeof(XmodemPacket_t) - 1;
  ret = uart_receiveBuffer(buf + 1,
                           requestedBytes,
                           &receivedBytes,
                           true,
                           3000);

  if (receivedBytes != requestedBytes) {
    // Didn't receive entire packet within 3000 ms; bail
    return BOOTLOADER_ERROR_COMMUNICATION_ERROR;
  }

  return ret;
}

int getAction(void)
{
  uint8_t c = 0;
  int ret = MENU;

  uart_receiveByte(&c);

  switch (c) {
    case '1':
      ret = INIT_TRANSFER;
      break;
    case '2':
      ret = BOOT;
      break;
    case '3':
      ret = SS2_PING;
      break;
    case '4':
      ret = SS2_FWDL_INITIATE;
      break;
    case '5':
      ret = SS2_FWDL_COMPLETE;
      break;
    default:
      ret = MENU;
      break;
  }
  return ret;
}

int main(void)
{
  // Coming out of reset...
  SystemCoreClock = SystemHfrcoFreq;

  // Stupid linker removes this structure and GBL converter fails...so make a reference
  transmitData[0] = applicationProperties.app.version;

  // Initialize the chip
  CHIP_Init();

  // Initialize the system clocks and other HAL components
  halInit();

  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO); //48MHz (RADIO)
  CMU_ClockEnable(cmuClock_HFPER, true); // (RADIO)

  // Initialize the BSP
  BSP_Init(BSP_INIT_BCC);

  // Initialize the LEDs on the board
  BSP_LedsInit();

  // Enable the buttons on the board
  for (int i = 0; i < BSP_NO_OF_BUTTONS; i++) {
    GPIO_PinModeSet(buttonArray[i].port, buttonArray[i].pin, gpioModeInputPull, 1);
  }

  // Button Interrupt Config
  GPIOINT_Init();
  GPIOINT_CallbackRegister(buttonArray[0].pin, gpioCallback);
  GPIOINT_CallbackRegister(buttonArray[1].pin, gpioCallback);
  GPIO_IntConfig(buttonArray[0].port, buttonArray[0].pin, false, true, true);
  GPIO_IntConfig(buttonArray[1].port, buttonArray[1].pin, false, true, true);

  // Init UART driver
  uart_init();

  // Initialize Radio
  radioInit();

  // Initialize delay timer
  delay_init();

  printf("Hello World version %ld!\r\n", applicationProperties.app.version);

  while (1) {
    switch (state)
    {
    case MENU:
      // Print menu
      menu();
      state = IDLE;
      break;

    case IDLE:
      // Get user input
      state = getAction();
      timeout = 60;
      break;

    case INIT_TRANSFER:
      printf("\r\nbegin upload\r\n");

      // Wait 5ms and see if we got any premature input; discard it
      delay_milliseconds(5, true);
      if (uart_getRxAvailableBytes()) {
        uart_flush(false, true);
      }

      // Initialize XMODEM parser
      xmodem_reset();

      state = WAIT_FOR_DATA;
      break;

    case WAIT_FOR_DATA:
      // Send 'C'
      sendPacket(XMODEM_CMD_C);
      delay_milliseconds(1000, false);
      while (uart_getRxAvailableBytes() == 0 && !delay_expired()) {
        // Do nothing
      }

      if (uart_getRxAvailableBytes()) {
        // We got a response; move to receive state
        FWDL_block_number = 0;
        state = RECEIVE_DATA;
      } else {
        // No response within 1 second; tick towards timeout
        timeout--;
        if (timeout == 0) {
          sendPacket(XMODEM_CMD_CAN);
          state = MENU;
        }
      }
      break;

    case RECEIVE_DATA:
      // Wait for a full XMODEM packet
      memset(&(buf.packet), 0, sizeof(XmodemPacket_t));
      ret = receivePacket(&(buf.packet));

      if (ret != BOOTLOADER_OK) {
        // Receiving packet failed -- timeout
        continue;
      }

      ret = xmodem_parsePacket(&(buf.packet), &response);
      if (ret == BOOTLOADER_ERROR_XMODEM_DONE) {
        // XMODEM receive complete; return to menu
        state = COMPLETE;

        // Send CAN rather than ACK if the image verification failed
        if (false/*FORNOW validate checksum...*/) {
          response = XMODEM_CMD_CAN;
        }
      }

      if ((ret == BOOTLOADER_OK) && (buf.packet.header == XMODEM_CMD_SOH)) {
        // Packet is OK, send contents
        // Send OTA image
        /*ret = */btl_FWDL_tx_code_block(buf.packet.data, XMODEM_DATA_SIZE, FWDL_block_number);
        delay_milliseconds(50, true);
        if (ret == BOOTLOADER_OK) {
        }
        else {
          // Parsing file failed; cancel transfer and return to menu
          response = XMODEM_CMD_CAN;
        }
        FWDL_block_number++;
      }

      if (response == XMODEM_CMD_CAN) {
        // Parsing packet failed; return to main menu
        state = COMPLETE;
      }

      // Send response
      sendPacket(response);
      break;

    case COMPLETE:
      uart_flush(false, true);

      delay_milliseconds(10, true);

      if ((response == XMODEM_CMD_ACK)
          && (ret == BOOTLOADER_ERROR_XMODEM_DONE)) {
        printf("\r\nSerial upload complete\r\n");
      } else {
        printf("\r\nSerial upload aborted\r\n");

        if ((ret >= BOOTLOADER_ERROR_XMODEM_BASE)
            && (ret < BOOTLOADER_ERROR_PARSER_BASE)) {
          printf("\r\nXModem block error 0x");
        } else {
          printf("\r\nfile error 0x");
        }

        switch (ret) {
          case BOOTLOADER_ERROR_XMODEM_NO_SOH:
            response = 0x21;
            break;
          case BOOTLOADER_ERROR_XMODEM_PKTNUM:
            response = 0x22;
            break;
          case BOOTLOADER_ERROR_XMODEM_CRCL:
            response = 0x23;
            break;
          case BOOTLOADER_ERROR_XMODEM_CRCH:
            response = 0x24;
            break;
          case BOOTLOADER_ERROR_XMODEM_PKTSEQ:
            response = 0x25;
            break;
          case BOOTLOADER_ERROR_XMODEM_PKTDUP:
            response = 0x27;
            break;

          case BOOTLOADER_ERROR_PARSER_VERSION:
            response = 0x41; // BL_ERR_HEADER_EXP
            break;
          case BOOTLOADER_ERROR_PARSER_CRC:
            response = 0x43; // BL_ERR_CRC
            break;
          case BOOTLOADER_ERROR_PARSER_UNKNOWN_TAG:
            response = 0x44; // BL_ERR_UNKNOWN_TAG
            break;
          case BOOTLOADER_ERROR_PARSER_SIGNATURE:
            response = 0x45; // BL_ERR_SIG
            break;
          case BOOTLOADER_ERROR_PARSER_FILETYPE:
          case BOOTLOADER_ERROR_PARSER_UNEXPECTED:
          case BOOTLOADER_ERROR_PARSER_EOF:
            response = 0x4F; // BL_ERR_TAGBUF
            break;
          case BOOTLOADER_ERROR_PARSER_KEYERROR:
            response = 0x50; // BL_ERR_INV_KEY
            break;
        }

        printf("%x\r\n", response);
      }
      state = MENU;
      break;

    case BOOT:
      printf("Not implemented\r\n");
      state = MENU;
      break;

    case SS2_PING:
      btl_ping();
      state = MENU;
      break;

    case SS2_FWDL_INITIATE:
      btl_FWDL_tx_initiate();
      state = MENU;
      break;

    case SS2_FWDL_COMPLETE:
      btl_FWDL_tx_complete();
      state = MENU;
      break;

    //case SS2_FWDL_RESTART:
    //  btl_FWDL_tx_restart();
    //  state = MENU;
    //  break;

    default:
      state = MENU;
      break;
    }

    if (packetTx) { // button press
      packetTx = false;
      // RAIL_Idle(railHandle, RAIL_IDLE, true);
      uint16_t dataLength = sizeof(transmitData);
      RAIL_WriteTxFifo(railHandle, &transmitData[0], dataLength, false);
      RAIL_StartTx(railHandle, channel, RAIL_TX_OPTIONS_DEFAULT, NULL);
    }
    // RX is handled automatically
    // ...
  }
}

/******************************************************************************
 * Configuration Utility Functions
 *****************************************************************************/
void radioInit()
{
  btl_radioInit();
}

void gpioCallback(uint8_t pin)
{
  static int n = 0;

  if (pin == 6)
  {
    putchar('0');
    if (n++ == 5) {
      bootloader_rebootAndInstall();
    }
  }
  else if (pin == 7)
  {
    putchar('1');
  }

  packetTx = true;
}
